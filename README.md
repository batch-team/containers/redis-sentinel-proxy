redis-sentinel-proxy
====================

Small command utility that:

* Given a redis sentinel server listening on `SENTINEL_ADDRESS`, keeps asking it for the address of a master named `NAME`

* Proxies all tcp requests that it receives on `IP:PORT` to that master


## Usage (CLI)

`./redis-sentinel-proxy -listen IP:PORT -sentinel SENTINEL_ADDRESS -master NAME`

## Usage (Kubernetes)

This service can be exposed behind a LB/Ingress in Kubernetes to allow simple external access to a redis sentinel based master-replica cluster

## Docker image

`Dockerfile` builds this repo as a docker image and publishes the images in the Gitlab Registry. 
It can then be used by dependent helm charts (redis-ha).

### Credits

Forked from https://github.com/flant/redis-sentinel-proxy
