FROM golang:1.16
MAINTAINER Ankur Singh <ankur.singh@cern.ch>
RUN mkdir /redis-sentinel-proxy
ADD . /redis-sentinel-proxy/
WORKDIR /redis-sentinel-proxy
RUN apt-get update
RUN apt-get install redis-tools -y
# fix https://github.com/golang/go/issues/31997
RUN go env -w GO111MODULE=auto
RUN go build -o redis-sentinel-proxy .
RUN mv /redis-sentinel-proxy/redis-sentinel-proxy /usr/local/bin/redis-sentinel-proxy

ENTRYPOINT ["/usr/local/bin/redis-sentinel-proxy"]
