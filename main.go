package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"time"

	"github.com/namsral/flag"
)

var (
	masterAddr *net.TCPAddr
	raddr      *net.TCPAddr
	saddr      *net.TCPAddr

	localAddr    = flag.String("listen", ":9999", "local address")
	sentinelAddr = flag.String("sentinel", ":26379", "remote address")
	masterName   = flag.String("master", "", "name of the master redis node")
	passwd       = flag.String("passwd", "", "auth password for redis (sentinel) node")
)

func main() {
	flag.Parse()

	laddr, err := net.ResolveTCPAddr("tcp", *localAddr)
	if err != nil {
		log.Fatal("failed to resolve local address: %s", err)
	}

	go master()

	listener, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Println(err)
			continue
		}

		go proxy(conn, masterAddr)
	}
}

func master() {
	var err error
	for {
		masterAddr, err = getMasterAddr(*sentinelAddr, *masterName, *passwd)
		if err != nil {
			log.Println(err)
		}
		time.Sleep(1 * time.Second)
	}
}

func pipe(r io.Reader, w io.WriteCloser) {
	io.Copy(w, r)
	w.Close()
}

func proxy(local io.ReadWriteCloser, remoteAddr *net.TCPAddr) {
	remote, err := net.DialTCP("tcp", nil, remoteAddr)
	if err != nil {
		log.Println(err)
		local.Close()
		return
	}
	go pipe(local, remote)
	go pipe(remote, local)
}

func getMasterAddr(sentinelAddr string, masterName string, passwd string) (*net.TCPAddr, error) {
	var err error

	// resolve DNS for sentinel everytime
	saddr, err = net.ResolveTCPAddr("tcp", sentinelAddr)
	if err != nil {
		log.Fatal("failed to resolve sentinel address: %s", err)
	}

	conn, err := net.DialTCP("tcp", nil, saddr)
	if err != nil {
		return nil, err
	}

	defer conn.Close()

	if len(passwd) > 0 {
		conn.Write([]byte(fmt.Sprintf("auth %s\n", passwd)))
		a := make([]byte, 256)
		_, err = conn.Read(a)
		if err != nil {
			log.Fatal(err)
		}
	}

	conn.Write([]byte(fmt.Sprintf("sentinel get-master-addr-by-name %s\n", masterName)))

	b := make([]byte, 256)
	_, err = conn.Read(b)
	if err != nil {
		log.Fatal(err)
	}

	parts := strings.Split(string(b), "\r\n")
	log.Println(parts)

	if len(parts) < 5 {
		log.Println(parts)
		err = errors.New("couldn't get master address from sentinel")
		return nil, err
	}

	//getting the string address for the master node
	stringaddr := fmt.Sprintf("%s:%s", parts[3], parts[5])
	addr, err := net.ResolveTCPAddr("tcp", stringaddr)

	if err != nil {
		return nil, err
	}

	//check that there's actually someone listening on that address
	conn2, err := net.DialTCP("tcp", nil, addr)
	if err == nil {
		defer conn2.Close()
	}

	return addr, err
}
